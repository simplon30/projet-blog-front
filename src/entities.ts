export interface Articles {
    id?:number;
    titre:string;
    image:string;
    contenu:string;
    date?:string;
    nombreDeVue:number;
}

export interface Commentaires {
    id?:number;
    user:string;
    contenu:string;
    date?:string;
    email:string;
    idArticles:number;
}