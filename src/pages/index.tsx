import AllArticles from "@/components/AllArticles";
import APropos from "@/components/APropos"
import BGImage from "@/components/BGImage";
import Footer from "@/components/Footer";
import Header from "@/components/Header";
import QuiSuisJe from "@/components/QuiSuisJe";

export default function Index() {

  return (
    <>
      <Header />

      < BGImage imgSRC="parallax2" />

      <APropos />

      <BGImage imgSRC="parallax1" />

      <AllArticles />

      <QuiSuisJe />

      <Footer />

    </>
  )

}