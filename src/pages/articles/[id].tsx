import { deleteArticles, deleteCommentaire, fetchAllCommentaire, fetchOneArticle, persistCommentaire, updateArticles } from "@/articles-service";
import Footer from "@/components/Footer";
import FormArticles from "@/components/FormArticles";
import Header from "@/components/Header";
import { Articles, Commentaires } from "@/entities";
import { useRouter } from "next/router";
import { FormEvent, useEffect, useState } from "react";

export default function lesArticles() {

    const router = useRouter();
    const { id } = router.query;
    const [article, setArticle] = useState<Articles>();

    const [commentaires, setCommentaires] = useState<Commentaires[]>([]);

    const [comments, setComments] = useState<Commentaires>({
        user: '',
        contenu: '',
        email: '',
        idArticles: Number(id)
    });

    const [showEdit, setShowEdit] = useState(false);

    const idtf = new Intl.DateTimeFormat("fr-FR", { year: "numeric", month: "long", day: "numeric" });

    let dateArticle = new Date();
    if (article?.date) {
        dateArticle = new Date(article.date)
    }

    async function remove() {
        await deleteArticles(id);
        router.push('/')
    }

    async function update(articles: Articles) {
        const updated = await updateArticles(articles)
        setArticle(updated)
        router.push('/articles/' + updated.id)
        toggle()
    }

    function toggle() {
        setShowEdit(!showEdit);
    }

    //Pour les Articles
    useEffect(() => {
        if (!id) {
            return
        }
        fetchOneArticle(Number(id))
            .then(data => setArticle(data))
    }, [id])

    //Pour les Commentaires
    useEffect(() => {
        fetchAllCommentaire()
            .then(data => setCommentaires(data));
    }, [])

    function handleChange(event: any) {
        setComments({
            ...comments,
            [event.target.name]: event.target.value
        });
    }

    async function post(event: FormEvent) {
        event.preventDefault();
        comments.idArticles = Number(id)
        const added = await persistCommentaire(comments);
        setCommentaires([...commentaires, added])
    }

    async function removeComment(id: number) {
        await deleteCommentaire(id);
        setCommentaires(commentaires.filter(item => item.id != id))
    }

    return (
        <>
            <Header />
            {/* affiche un article */}
            <article>

                <h1 className="title">{article?.titre}</h1>

                <button className="modifier m-3 btn btn-dark" onClick={toggle}>Modifier</button>
                <button className="remove m-3 btn btn-dark" onClick={remove}>Supprimer</button>

                {showEdit &&
                    <>
                        <FormArticles txt="Modifier l'Article" edited={article} onSubmit={update} />
                    </>}

                <img className="img-fluid img" src={article?.image} alt={article?.titre} />
                <p className="txt">{article?.contenu}</p>
                <div className="dateVue">

                    <p>{idtf.format(dateArticle)}</p>
                    <p>Nombre de vue : {article?.nombreDeVue}</p>
                </div>

                {commentaires.map(item => item.idArticles == article?.id &&
                    <>
                        <div className="card">
                            <div className="card-body">
                                <h5 className="card-title">{item.user}</h5>
                                <button onClick={() => removeComment(item.id!)} type="button" className="btn-close remove" aria-label="Close"></button>
                                <p className="card-subtitle text-muted">{idtf.format(new Date(String(item.date)))}</p>
                                <p className="card-text">{item.contenu}</p>
                            </div>
                        </div>
                    </>
                )}

                <form onSubmit={post}>

                    <div className="mb-3 formArticles">
                        <label className="form-label" htmlFor="user">Ton pseudo : </label>
                        <input className="form-control" type="text" name="user" value={comments.user} onChange={handleChange} />

                        <label className="form-label" htmlFor="contenu">Ton Message </label>
                        <input className="form-control" type="text" name="contenu" value={comments.contenu} onChange={handleChange} />

                        <button className="mt-3 btn btn-dark">Poster</button>
                    </div>
                </form>

            </article>
            
            <Footer />
        </>
    )
}