import { persistArticles } from "@/articles-service";
import Footer from "@/components/Footer";
import FormArticles from "@/components/FormArticles";
import Header from "@/components/Header";
import { Articles } from "@/entities";
import { useRouter } from "next/router";

export default function AddArticles() {

    const router = useRouter();

    async function addArticles(articles: Articles) {
        const added = await persistArticles(articles);
        router.push('/articles/' + added.id)
    }

    return (
        <>
        <Header />
            <FormArticles txt="Ajouter l'Article" onSubmit={addArticles} />
        <Footer />
        </>
    )
}