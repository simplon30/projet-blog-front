import Link from 'next/link'
import { useRouter } from 'next/router';
import { useEffect, useState } from 'react';

export default function FourOhFour() {

    const [redirectSeconds, setRedirectSeconds] = useState<number>(10);
    const router = useRouter();

    useEffect(() => {
        if (redirectSeconds == 0) {
            router.push("https://shattereddisk.github.io/rickroll/rickroll.mp4");
            return;
        }
        setTimeout(() => {
            console.log(redirectSeconds);
            setRedirectSeconds((redirectSeconds) => redirectSeconds - 1);
        }, 1000)
    }, [redirectSeconds]);

    return <>
        <h2 className='notFoundtxt'>404 - Page Not Found</h2>
        <div className='notFoundtxt'>
            Vous serez redirigé dans ... {redirectSeconds} secondes.
        </div>
        <img className='notFoundIMG' src="https://cdnb.artstation.com/p/assets/images/images/039/280/851/large/alexandre-hildans-vampire-artbook.jpg?1625475486" alt="" />
        <Link className='notFoundtxt' href="">
            Go back home
        </Link>
    </>
}