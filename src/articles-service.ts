import axios from "axios";
import { Articles, Commentaires } from "./entities";



export async function fetchAllArticles(){
    const response = await axios.get<Articles[]>('http://localhost:8000/api/articles');
    return response.data;
}

export async function fetchOneArticle(id : number){
    const response = await axios.get<Articles>('http://localhost:8000/api/articles/'+id);
    return response.data;
}

export async function persistArticles(articles : Articles){
    const response = await axios.post<Articles>('http://localhost:8000/api/articles', articles);
    return response.data;
};

export async function deleteArticles(id:any) {
    await axios.delete('http://localhost:8000/api/articles/'+id);
}

export async function updateArticles(articles : Articles){
    const response = await axios.put<Articles>('http://localhost:8000/api/articles/'+articles.id, articles);
    return response.data;
}

export async function fetchAllCommentaire(){
    const response = await axios.get<Commentaires[]>('http://localhost:8000/api/commentaire');
    return response.data;
}

export async function persistCommentaire(commentaires : Commentaires){
    const response = await axios.post<Commentaires>('http://localhost:8000/api/commentaire', commentaires);
    return response.data;
};

export async function deleteCommentaire(id:any){
    await axios.delete('http://localhost:8000/api/commentaire/'+id);
};