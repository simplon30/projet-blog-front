import { fetchAllArticles } from "@/articles-service";
import { Articles } from "@/entities";
import { useEffect, useState } from "react";
import ItemArticles from "./ItemArticles";


export default function AllArticles() {

    const [articles, setArticles] = useState<Articles[]>([]);

    useEffect(() => {

        fetchAllArticles().then(data => {
            setArticles(data);
        });

    }, [])

    return (
        <>
            <article className="mx-auto text-center align-middle lastArticles">
                <h2 className="pt-5">Les Derniers Articles</h2>
                <h4>____________</h4>
                <div className="container">
                    <div className="row">
                        {articles && articles.map(item =>
                            <ItemArticles key={item.id} articles={item} />
                        )}
                    </div>
                </div>
            </article>
        </>
    )
}