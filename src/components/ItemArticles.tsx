import { Articles } from "@/entities";

interface Props {
    articles: Articles;
}


export default function ItemArticles({ articles }: Props) {

    function pouet() {
        const url = "articles/" + articles.id
        window.open(url, '_self');
    }

    return (
        <>
            <div className="col-md-6">
                <div className="card mb-5 mt-5" style={{ width: "", height: "auto" }}>
                    <img src={articles.image} className="card-img-top" alt="..." />
                    <div className="card-body">
                        <h5 className="card-title">{articles.titre}</h5>
                        <a onClick={pouet} className="btn btn-dark">Voir Plus</a>
                    </div>
                </div>
            </div>
        </>
    );

}

