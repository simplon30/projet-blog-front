
interface Props {
    imgSRC: any;
}


export default function BGImage({ imgSRC }: Props) {
    return (
        <>
            <div className={"parallax " + imgSRC}></div>
        </>
    )
}