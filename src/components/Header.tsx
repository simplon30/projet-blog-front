import Link from "next/link";

export default function Header() {

    return (
        <nav className="header navbar navbar-expand-lg navbar-light bg-light">

            <div className="mx-auto">

                <div className="text-center">
                    <Link className="navbar-brand fs-1 " href="/">Alexandre Hildans</Link>
                </div>

                <nav className="navbar navbar-expand-lg bg-body-tertiary">
                    <div className="container-fluid">
                        <div className="mx-auto text-center">

                            <button className="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
                                <span className="navbar-toggler-icon"></span>
                            </button>
                        </div>

                        <div className="collapse navbar-collapse" id="navbarNav">
                            <ul className="navbar-nav">
                                <li className="nav-item">
                                    <Link className="nav-link active" aria-current="page" href="/">Accueil</Link>
                                </li>
                                <li className="nav-item">
                                    <Link className="nav-link" href="/articles/add">Créer un Article</Link>
                                </li>
                                <li className="nav-item">
                                    <Link className="nav-link" href="#footer">Contact</Link>
                                </li>

                                <div className="nav-item dropdown">
                                    <Link className="nav-link dropdown-toggle" href="#" role="button" data-bs-toggle="dropdown" aria-expanded="false">
                                        Réseaux
                                    </Link>
                                    <ul className="dropdown-menu">
                                        <li><Link className="dropdown-item" target={"_blank"} href="https://www.instagram.com/eksandr_hildans/">Instagram</Link></li>
                                        <li><Link className="dropdown-item" target={"_blank"} href="https://www.artstation.com/eksandr">ArtStation</Link></li>
                                        <li><Link className="dropdown-item" target={"_blank"} href="https://www.youtube.com/watch?v=gn6-FYRszm4&ab_channel=EksandrHildans">Youtube</Link></li>
                                    </ul>
                                </div>
                            </ul>
                        </div>
                    </div>
                </nav>

            </div>
        </nav>
    )
}