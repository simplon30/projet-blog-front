import Link from "next/link";

export default function Footer() {
    return (
        <>
            <footer>
                <div className="footer" id="footer">
                    <p>
                        <Link className="text-decoration-none circle" target={"_blank"} href="https://www.instagram.com/eksandr_hildans/">IG</Link>
                    </p>
                    <p>
                        <Link className="text-decoration-none circle" target={"_blank"} href="https://www.artstation.com/eksandr">ArtS</Link>
                    </p>
                    <p>
                        <Link className="text-decoration-none circle" target={"_blank"} href="https://www.youtube.com/watch?v=gn6-FYRszm4&ab_channel=EksandrHildans">YT</Link>
                    </p>
                </div>
                <p className="PeC">Politique et Confidentialité</p>
            </footer>
        </>
    )
}