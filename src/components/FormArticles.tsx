import { Articles } from "@/entities";
import { FormEvent, useState } from "react";

interface Props {
    onSubmit: (articles: Articles) => void;
    edited?: Articles
    txt: string
}


export default function FormArticles({ onSubmit, edited, txt }: Props) {

    const [articles, setArticles] = useState<Articles>(edited ? edited : {
        titre: '',
        image: '',
        contenu: '',
        nombreDeVue: 0
    });

    function handleChange(event: any) {
        setArticles({
            ...articles,
            [event.target.name]: event.target.value
        });
    }

    async function addArticles(event: FormEvent) {
        event.preventDefault();
        onSubmit(articles)
    }

    return (
        <>
            <form onSubmit={addArticles}>

                <div className="mb-3 formArticles">
                    <label className="form-label" htmlFor="titre">Titre : </label>
                    <input className="form-control" type="text" name="titre" value={articles.titre} onChange={handleChange} />

                    <label className="form-label" htmlFor="image">url de l'image : </label>
                    <input className="form-control" type="text" name="image" value={articles.image} onChange={handleChange} />

                    <label className="form-label" htmlFor="contenu">Contenu : </label>
                    <textarea className="form-control formContenu" name="contenu" value={articles.contenu} onChange={handleChange} />

                    <button className="mt-3 btn btn-dark">{txt}</button>
                </div>
            </form>
        </>
    )
}